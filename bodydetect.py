from imageai.Detection import ObjectDetection
import os
import sys

execution_path = os.getcwd()

detector = ObjectDetection()
detector.setModelTypeAsYOLOv3()
detector.setModelPath( os.path.join(execution_path , "yolo.h5"))
detector.loadModel()

custom_objects = detector.CustomObjects(person=True)
detections = detector.detectCustomObjectsFromImage(custom_objects=custom_objects, input_image=os.path.join(execution_path , sys.argv[1]), output_image_path=os.path.join(execution_path , "testhuman.jpg"), minimum_percentage_probability=10)


#detections = detector.detectObjectsFromImage(input_image=os.path.join(execution_path , "/Users/gasprp/Desktop/projectpic/IMG_4281.jpeg"), output_image_path=os.path.join(execution_path , "testdetect4.jpeg"), minimum_percentage_probability=30)
for eachObject in detections:
    print(eachObject["name"] , " : ", eachObject["percentage_probability"], " : ", eachObject["box_points"])
