package img;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
public class Hw12A {
	
	public Hw12A() {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		 Mat kern = new Mat(3, 3, CvType.CV_8S);
         int row = 0, col = 0;
         kern.put(row, col, 0, -1, 0, -1, 5, -1, 0, -1, 0);
		Mat mat = Imgcodecs.imread("/Users/gasprp/eclipse-workspace/img/Hw1_2A.jpg", Imgcodecs.IMREAD_GRAYSCALE);
		Mat result = mat.clone();
		
		
		Mat gaussian = new Mat();
		Imgproc.medianBlur(mat, gaussian, 3);
	//	Imgcodecs.imwrite("Hw1_2A_6009610632_1.jpg", gaussian);
		Mat grad_x = new Mat();
		Mat grad_y = new Mat();

		Mat abs_grad_x = new Mat();
		Mat abs_grad_y = new Mat();

		Imgproc.Sobel(gaussian, grad_x, CvType.CV_16S, 1, 0, 3, 1, 0, Core.BORDER_DEFAULT);
		Core.convertScaleAbs(grad_x, abs_grad_x);
		
		Imgproc.Sobel(gaussian, grad_y, CvType.CV_16S, 0, 1, 3, 1, 0, Core.BORDER_DEFAULT);
		Core.convertScaleAbs(grad_y, abs_grad_y);
		
		Mat grad = new Mat();
		Core.addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0, grad);
		//Imgcodecs.imwrite("Hw1_2A_6009610632_2.jpg", grad);
		Mat gradN = new Mat();
		Mat Ngrad = new Mat();
		Core.bitwise_not(grad, Ngrad);
		//Imgcodecs.imwrite("Hw1_2A_6009610632_3.jpg", Ngrad);
		Core.addWeighted(gaussian, 1, Ngrad, 0.2, 0, gradN);
		
		//HighGui.imshow("result", gradN);
		//Imgcodecs.imwrite("Hw1_2A_6009610632_4.jpg", gradN);
		//HighGui.waitKey();
		
		
		// Sobel
//		Mat resultSobel1 = new Mat();
//		Mat resultSobel2 = new Mat();
//		Mat resultSobel = new Mat();
//		Imgproc.Sobel(gradN, resultSobel1, result.depth(), 0, 1,1);
//		Imgproc.Sobel(gradN, resultSobel2, result.depth(), 1, 0,1);
//		Core.addWeighted(resultSobel1, 0.5, resultSobel2, 0.5, 0, resultSobel);
//		Imgcodecs.imwrite("Hw1_2A_6009610632_Sobel1.jpg", resultSobel);
//        HighGui.imshow("prewitH", resultSobel);
//        HighGui.waitKey();
		

		// Prewit
//	    Mat kernH = new Mat(3, 3, CvType.CV_8S);
//        kernH.put(0, 0, -1, -1, -1, 0, 0, 0, 1, 1, 1);
//        Mat prewittH = new Mat();
//        Imgproc.filter2D(gradN, prewittH, result.depth(), kernH);
//		Mat kernV = new Mat(3, 3, CvType.CV_8S);
//		kernV.put(0, 0, -1, 0, 1, -1, 0, 1, -1, 0, 1);
//		Mat prewittV = new Mat();
//		Imgproc.filter2D(gradN, prewittV, result.depth(), kernV);
//		Mat resultPrewitt = new Mat();
//		Core.addWeighted(prewittH, 0.5, prewittV, 0.5, 0, resultPrewitt);
//		Imgcodecs.imwrite("Hw1_2A_6009610632_Prewitt.jpg", resultPrewitt);
//		HighGui.imshow("prewitV", resultPrewitt);
//		HighGui.waitKey();
//		
		//Canny
//		Mat resultCanny = new Mat();
//		Imgproc.Canny(gradN,resultCanny,100, 200);
//		HighGui.imshow("Canny", resultCanny);
//		Imgcodecs.imwrite("Hw1_2A_6009610632_Canny50.jpg", resultCanny);
//		HighGui.waitKey();
		
		System.exit(0);

	}
	public static void main(String[] args) {
		new Hw12A();
	}

}
