package img;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class Hw12B {

	public Hw12B() {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

		Mat image = Imgcodecs.imread("/Users/gasprp/eclipse-workspace/img/Hw1_2B.jpg", Imgcodecs.IMREAD_GRAYSCALE);
		Mat newImage = new Mat();
		image.convertTo(newImage, -1, 0.9, 0);
		// Imgcodecs.imwrite("Hw1_2B_6009610632_1.jpg", newImage);
		Mat result = newImage.clone();
		for (int i = 0; i < result.rows() - 1; i++) {
			for (int l = 0; l < result.cols() - 1; l++) {
				double[] data = result.get(i, l);
				for (int x = 0; x < data.length; x++) {
					if (data[x] >= 210) {
						data[x] = data[x] - 25;
					}
					if (data[x] < 0) {
						data[x] = 0;
					}
					if (data[x] > 255) {
						data[x] = 255;
					}

				}

				result.put(i, l, data);
			}
		}

		// HighGui.imshow("new Img", result);
		// Imgcodecs.imwrite("Hw1_2B_6009610632_2.jpg", newImage);
		// HighGui.waitKey();

		// Sobel
//		Mat resultSobel1 = new Mat();
//		Mat resultSobel2 = new Mat();
//		Mat resultSobel = new Mat();
//		Imgproc.Sobel(result, resultSobel1, result.depth(), 0, 1,5);
//		Imgproc.Sobel(result, resultSobel2, result.depth(), 1, 0,5);
//		Core.addWeighted(resultSobel1, 0.5, resultSobel2, 0.5, 0, resultSobel);
		//Imgcodecs.imwrite("Hw1_2B_6009610632_Sobel5.jpg", resultSobel);
       // HighGui.imshow("prewitH", resultSobel);
       // HighGui.waitKey();
		

		// Prewit
//	    Mat kernH = new Mat(3, 3, CvType.CV_8S);
//        kernH.put(0, 0, -1, -1, -1, 0, 0, 0, 1, 1, 1);
//        Mat prewittH = new Mat();
//        Imgproc.filter2D(result, prewittH, result.depth(), kernH);
//		Mat kernV = new Mat(3, 3, CvType.CV_8S);
//		kernV.put(0, 0, -1, 0, 1, -1, 0, 1, -1, 0, 1);
//		Mat prewittV = new Mat();
//		Imgproc.filter2D(result, prewittV, result.depth(), kernV);
//		Mat resultPrewitt = new Mat();
//		Core.addWeighted(prewittH, 0.5, prewittV, 0.5, 0, resultPrewitt);
//		Imgcodecs.imwrite("Hw1_2B_6009610632_Prewitt.jpg", resultPrewitt);
//		HighGui.imshow("prewitV", resultPrewitt);
//		HighGui.waitKey();
		
		//Canny
//		Mat resultCanny = new Mat();
//		Imgproc.Canny(result,resultCanny,100, 200);
//		HighGui.imshow("Canny", resultCanny);
//		Imgcodecs.imwrite("Hw1_2B_6009610632_Canny200.jpg", resultCanny);
//		HighGui.waitKey();
		System.exit(0);
		
	}

	public static void main(String[] args) {
		new Hw12B();
	}

}
