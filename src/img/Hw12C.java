package img;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class Hw12C {

	public Hw12C() {

		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		
		Mat mat = Imgcodecs.imread("/Users/gasprp/eclipse-workspace/img/Hw1_2C.jpg", Imgcodecs.IMREAD_GRAYSCALE);
		Mat smootM = new Mat();
		Imgproc.medianBlur(mat, smootM, 5);
		Imgcodecs.imwrite("Hw1_2C_6009610632_1.jpg", smootM);
		Mat result = new Mat();
		Mat kern = new Mat(3, 3, CvType.CV_8S);
		kern.put(0, 0, 0, -1, 0, -1, 5, -1, 0, -1, 0);
		Imgproc.filter2D(smootM, result, smootM.depth(), kern);
		HighGui.imshow("result", result);
		Imgcodecs.imwrite("Hw1_2C_6009610632_2.jpg", result);
		HighGui.waitKey();
		
		
		
		// Sobel
//		Mat resultSobel1 = new Mat();
//		Mat resultSobel2 = new Mat();
//		Mat resultSobel = new Mat();
//		Imgproc.Sobel(result, resultSobel1, result.depth(), 0, 1,5);
//		Imgproc.Sobel(result, resultSobel2, result.depth(), 1, 0,5);
//		Core.addWeighted(resultSobel1, 0.5, resultSobel2, 0.5, 0, resultSobel);
//		Imgcodecs.imwrite("Hw1_2C_6009610632_Sobel5.jpg", resultSobel);
//        HighGui.imshow("prewitH", resultSobel);
//        HighGui.waitKey();
		

		// Prewit
//	    Mat kernH = new Mat(3, 3, CvType.CV_8S);
//        kernH.put(0, 0, -1, -1, -1, 0, 0, 0, 1, 1, 1);
//        Mat prewittH = new Mat();
//        Imgproc.filter2D(result, prewittH, result.depth(), kernH);
//		Mat kernV = new Mat(3, 3, CvType.CV_8S);
//		kernV.put(0, 0, -1, 0, 1, -1, 0, 1, -1, 0, 1);
//		Mat prewittV = new Mat();
//		Imgproc.filter2D(result, prewittV, result.depth(), kernV);
//		Mat resultPrewitt = new Mat();
//		Core.addWeighted(prewittH, 0.5, prewittV, 0.5, 0, resultPrewitt);
//		Imgcodecs.imwrite("Hw1_2C_6009610632_Prewitt.jpg", resultPrewitt);
//		HighGui.imshow("prewitV", resultPrewitt);
//		HighGui.waitKey();
		
		//Canny
//		Mat resultCanny = new Mat();
//		Imgproc.Canny(result,resultCanny,0, 50);
//		HighGui.imshow("Canny", resultCanny);
//		Imgcodecs.imwrite("Hw1_2C_6009610632_Canny50.jpg", resultCanny);
//		HighGui.waitKey();
		System.exit(0);
			
	}

	public static void main(String[] args) {
		new Hw12C();

	}

}
