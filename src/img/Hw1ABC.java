package img;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class Hw1ABC {

	public Hw1ABC() {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		Mat mat = Imgcodecs.imread("/Users/gasprp/eclipse-workspace/img/Hw1_1A.JPG", Imgcodecs.IMREAD_GRAYSCALE);

		Mat result = mat.clone();
		for (int i = 0; i < result.rows() - 1; i++) {
			for (int l = 0; l < result.cols() - 1; l++) {
				double[] data = result.get(i, l);
				for (int x = 0; x < data.length; x++) {
					//data[x] = Math.round(30*(Math.log(data[x]+1)));
					//data[x] = Math.round(3* (Math.pow(data[x], 0.8)));
					data[x] = Math.round(1*(Math.pow(data[x],1.02)));
					
					if (data[x] < 0) {
						data[x] = 0;
					}
					if (data[x] > 255) {
						data[x] = 255;
					}

				}

				result.put(i, l, data);
			}
		}
		Imgcodecs.imwrite("Hw1_1A_6009610632_C.jpg",result);
		HighGui.imshow("Test", result);
		HighGui.waitKey();
		System.exit(0);
		

	}

	public static void main(String[] args) {
		//new Hw1ABC();
		
	}

}
