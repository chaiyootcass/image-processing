package img;

import java.util.HashMap;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class Hw2_2B {

	public Hw2_2B() {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		String filename = "/Users/gasprp/eclipse-workspace/img/circles.JPG";
		Mat src = Imgcodecs.imread(filename, Imgcodecs.IMREAD_GRAYSCALE);
		if (src.empty()) {
			System.err.println("Cannot read image: " + filename);
			System.exit(0);
		}
		Mat result = src.clone();
		// Segment
		for (int i = 0; i < result.rows(); i++) {
			for (int l = 0; l < result.cols(); l++) {
				double[] data = result.get(i, l);
				for (int x = 0; x < data.length; x++) {
					if (data[x] <= 65) {
						data[x] = 255;
					}
				}
				result.put(i, l, data);
			}
		}
		Imgcodecs.imwrite("Hw2i2_6009610632_1.jpg", result);
		for (int i = 0; i < result.rows(); i++) {
			for (int l = 0; l < result.cols(); l++) {
				double[] data = result.get(i, l);
				for (int x = 0; x < data.length; x++) {
					if (data[x] != 255) {
						data[x] = data[x] + 35;
					}
				}
				result.put(i, l, data);
			}
		}
		Imgcodecs.imwrite("Hw2i2_6009610632_2.jpg", result);
		for (int i = 0; i < result.rows(); i++) {
			for (int l = 0; l < result.cols(); l++) {
				double[] data = result.get(i, l);
				for (int x = 0; x < data.length; x++) {
					if (data[x] != 255) {
						data[x] = 0;
					}
				}
				result.put(i, l, data);
			}
		}
		Imgcodecs.imwrite("Hw2i2_6009610632_3.jpg", result);
		// Labeling
		int labelCount = 1;
		HashMap<Integer, Integer> x = new HashMap<Integer, Integer>();
		System.out.println(result.width() + " " + result.height());
		int[][] multi = new int[16][16];
		for (int i = 0; i < 16; i++) {
			for (int l = 0; l < 16; l++) {
				multi[i][l] = 0;
				for (int i2 = i * 16; i2 < (i * 16) + 16; i2++) {
					for (int l2 = l * 16; l2 < (l * 16) + 16; l2++) {
						if (result.get(i2, l2)[0] == 0) {
							multi[i][l] = 1;
						}
					}
				}
			}
		}

		for (int i = 0; i < multi.length; i++) {
			for (int l = 0; l < multi.length; l++) {
				int p = multi[i][l];
				if (p == 1 && i == 0 && l == 0) {
					multi[i][l] = labelCount;
					labelCount++;
				} else if (p == 1 && l == 0) {
					if (multi[i - 1][l] != 0) {
						multi[i][l] = multi[i - 1][l];
					} else {
						multi[i][l] = labelCount;
						labelCount++;
					}
				} else if (p == 1 && i == 0) {
					if (multi[i][l - 1] != 0) {
						multi[i][l] = multi[i][l - 1];
					} else {
						multi[i][l] = labelCount;
						labelCount++;
					}
				} else if (p == 1 && i != 0 && l != 0) {
					if (multi[i][l - 1] != 0 && multi[i - 1][l] != 0) {
						if (multi[i - 1][l] > multi[i][l - 1]) {
							multi[i][l] = multi[i][l - 1];
							x.put(multi[i - 1][l], multi[i][l - 1]);
						} else if (multi[i - 1][l] < multi[i][l - 1]) {
							multi[i][l] = multi[i - 1][l];
							x.put(multi[i][l - 1], multi[i - 1][l]);
						} else {
							multi[i][l] = multi[i - 1][l];
						}

					} else if (multi[i][l - 1] == 0 && multi[i - 1][l] != 0) {
						multi[i][l] = multi[i - 1][l];
					} else if (multi[i - 1][l] == 0 && multi[i][l - 1] != 0) {
						multi[i][l] = multi[i][l - 1];
					} else {
						multi[i][l] = labelCount;
						labelCount++;
					}
				}

			}
		}
		x.forEach((key, valuse) -> {
			System.out.println(key + " -> " + valuse);
		});
		for (int i = 0; i < multi.length; i++) {
			for (int l = 0; l < multi.length; l++) {
				System.out.print(multi[i][l]);
			}
			System.out.println();
		}
		for (int z = 0; z < x.size(); z++) {
			for (int i = 0; i < multi.length; i++) {
				for (int l = 0; l < multi.length; l++) {
					if (x.containsKey(multi[i][l])) {
						multi[i][l] = x.get(multi[i][l]);
					}
				}
			}
		}

		for (int i = 0; i < multi.length; i++) {
			for (int l = 0; l < multi.length; l++) {
				System.out.print(multi[i][l]);
			}
			System.out.println();
		}

		// HighGui.imshow("Test", result);

		// HighGui.waitKey();
		// System.exit(0);

	}

	public static void main(String[] args) {
		new Hw2_2B();

	}

}
