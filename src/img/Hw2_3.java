package img;

import java.util.HashMap;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Size;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class Hw2_3 {

	public Hw2_3() {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		String filename = "/Users/gasprp/eclipse-workspace/img/ani01.jpg";
		Mat src = Imgcodecs.imread(filename, Imgcodecs.IMREAD_GRAYSCALE);

		if (src.empty()) {
			System.err.println("Cannot read image: " + filename);
			System.exit(0);
		}
		Mat result = src.clone();
		Mat afterT = new Mat();

		Imgproc.threshold(result, afterT, 0, 255, Imgproc.THRESH_TRIANGLE);
		int erosion_size = 2;
		int dilation_size = 2;
		// Imgcodecs.imwrite("Hw2i3_6009610632_1.jpg", afterT);

		Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT,
				new Size(2 * erosion_size + 1, 2 * erosion_size + 1));
		Mat afterE = new Mat();
		Imgproc.dilate(afterT, afterE, element);
		// Imgcodecs.imwrite("Hw2i3_6009610632_2.jpg", afterE);
		int erosion_size2 = 1;
		int dilation_size2 = 1;

		Mat element2 = Imgproc.getStructuringElement(Imgproc.MORPH_RECT,
				new Size(2 * erosion_size2 + 1, 2 * erosion_size2 + 1));
		Mat afterE2 = new Mat();
		Imgproc.dilate(afterE, afterE2, element);
		// Imgcodecs.imwrite("Hw2i3_6009610632_3.jpg", afterE2);

		Imgproc.medianBlur(afterE2, afterE2, 7);
		Imgproc.dilate(afterE2, afterE2, element2);
		Imgproc.medianBlur(afterE2, afterE2, 3);

		// Imgcodecs.imwrite("Hw2i3_6009610632_4.jpg", afterE2);

		int labelCount = 1;
		HashMap<Integer, Integer> x = new HashMap<Integer, Integer>();
		System.out.println(afterE2.width() + " " + afterE2.height());
		int[][] multi = new int[111][192];
		for (int i = 0; i < 111; i++) {
			for (int l = 0; l < 192; l++) {
				multi[i][l] = 0;
				for (int i2 = i * 5; i2 < (i * 5) + 5; i2++) {
					for (int l2 = l * 5; l2 < (l * 5) + 5; l2++) {
						if (afterE2.get(i2, l2)[0] <= 150) {
							multi[i][l] = 1;
						}
					}
				}
			}
		}
		for (int i = 0; i < afterE2.height() / 5; i++) {
			for (int l = 0; l < afterE2.width() / 5; l++) {
				int p = multi[i][l];
				if (p == 1 && i == 0 && l == 0) {
					multi[i][l] = labelCount;
					labelCount++;
				} else if (p == 1 && l == 0) {
					if (multi[i - 1][l] != 0) {
						multi[i][l] = multi[i - 1][l];
					} else {
						multi[i][l] = labelCount;
						labelCount++;
					}
				} else if (p == 1 && i == 0) {
					if (multi[i][l - 1] != 0) {
						multi[i][l] = multi[i][l - 1];
					} else {
						multi[i][l] = labelCount;
						labelCount++;
					}
				} else if (p == 1 && i != 0 && l != 0) {
					if (multi[i][l - 1] != 0 && multi[i - 1][l] != 0) {
						if (multi[i - 1][l] > multi[i][l - 1]) {
							multi[i][l] = multi[i][l - 1];
							x.put(multi[i - 1][l], multi[i][l - 1]);
						} else if (multi[i - 1][l] < multi[i][l - 1]) {
							multi[i][l] = multi[i - 1][l];
							x.put(multi[i][l - 1], multi[i - 1][l]);
						} else {
							multi[i][l] = multi[i - 1][l];
						}

					} else if (multi[i][l - 1] == 0 && multi[i - 1][l] != 0) {
						multi[i][l] = multi[i - 1][l];
					} else if (multi[i - 1][l] == 0 && multi[i][l - 1] != 0) {
						multi[i][l] = multi[i][l - 1];
					} else {
						multi[i][l] = labelCount;
						labelCount++;
					}
				}

			}
		}
		x.forEach((key, valuse) -> {
			System.out.println(key + "  " + valuse);
		});
		System.out.println("Labelcount " + labelCount + " x " + x.size());
		for (int i = 0; i < 111; i++) {
			for (int l = 0; l < 192; l++) {
				if (x.containsKey(multi[i][l])) {
					multi[i][l] = x.get(multi[i][l]);
				}
			}
		}
		for (int i = 0; i < 111; i++) {
			for (int l = 0; l < 192; l++) {
				if (x.containsKey(multi[i][l])) {
					multi[i][l] = x.get(multi[i][l]);
				}
			}
		}
		for (int i = 0; i < 111; i++) {
			for (int l = 0; l < 192; l++) {
				if (x.containsKey(multi[i][l])) {
					multi[i][l] = x.get(multi[i][l]);
				}
			}
		}
		for (int i = 0; i < 111; i++) {
			for (int l = 0; l < 192; l++) {
				System.out.print(multi[i][l]);
			}
			System.out.println();
		}

		HighGui.imshow("Test", afterE2);

		// Imgcodecs.imwrite("Hw2i3_6009610632_1.jpg", result);
		HighGui.waitKey();
		System.exit(0);
	}

	public static void main(String[] args) {
		new Hw2_3();

	}

}
