package img;

import java.util.HashMap;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class Hw2_32 {

	public Hw2_32() {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		String filename = "/Users/gasprp/eclipse-workspace/img/ani02.jpg";
		Mat src = Imgcodecs.imread(filename, Imgcodecs.IMREAD_GRAYSCALE);

		if (src.empty()) {
			System.err.println("Cannot read image: " + filename);
			System.exit(0);
		}
		Mat result = src.clone();
		Mat afterT = new Mat();

		for (int i = 0; i < result.rows(); i++) {
			for (int l = 0; l < result.cols(); l++) {
				double[] data = result.get(i, l);
				for (int x = 0; x < data.length; x++) {
					if (data[x] < 40) {
						data[x] = 0;
					} else {
						data[x] = 255;
					}
				}
				result.put(i, l, data);
			}
		}
		// Imgcodecs.imwrite("Hw2i3_6009610632_5.jpg", result);

		int erosion_size = 1;
		int dilation_size = 1;

		Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT,
				new Size(2 * erosion_size + 1, 2 * erosion_size + 1));
		Mat afterE = new Mat();
		Imgproc.dilate(result, result, element);
		// Imgcodecs.imwrite("Hw2i3_6009610632_6.jpg", result);

		Imgproc.medianBlur(result, result, 3);
		Imgproc.medianBlur(result, result, 3);
		Imgproc.medianBlur(result, result, 3);
		//Imgcodecs.imwrite("Hw2i3_6009610632_7.jpg", result);
		int erosion_size2 = 7;
		int dilation_size2 = 7;

		Mat element2 = Imgproc.getStructuringElement(Imgproc.MORPH_RECT,
				new Size(2 * erosion_size2 + 1, 2 * erosion_size2 + 1));

		Imgproc.erode(result, result, element);
		Imgproc.erode(result, result, element);
		Imgproc.erode(result, afterE, element);
		//Imgcodecs.imwrite("Hw2i3_6009610632_8.jpg", afterE);

		for (int i = 0; i < afterE.rows(); i++) {
			for (int l = 0; l < afterE.cols(); l++) {
				double[] data = afterE.get(i, l);
				for (int x = 0; x < data.length; x++) {
					if (data[x] != 255) {
						data[x] = 0;
					} else {
						data[x] = 255;
					}
				}
				afterE.put(i, l, data);
			}
		}
		int labelCount = 1;
		HashMap<Integer, Integer> x = new HashMap<Integer, Integer>();
		System.out.println(afterE.width() + " " + afterE.height());

		int[][] multi = new int[153][240];
		for (int i = 0; i < 153; i++) {
			for (int l = 0; l < 240; l++) {
				multi[i][l] = 0;
				for (int i2 = i * 5; i2 < (i * 5) + 5; i2++) {
					for (int l2 = l * 5; l2 < (l * 5) + 5; l2++) {
						if (afterE.get(i2, l2)[0] <= 150) {
							multi[i][l] = 1;
						}
					}
				}
			}
		}
		for (int i = 0; i < afterE.height() / 5; i++) {
			for (int l = 0; l < afterE.width() / 5; l++) {
				int p = multi[i][l];
				if (p == 1 && i == 0 && l == 0) {
					multi[i][l] = labelCount;
					labelCount++;
				} else if (p == 1 && l == 0) {
					if (multi[i - 1][l] != 0) {
						multi[i][l] = multi[i - 1][l];
					} else {
						multi[i][l] = labelCount;
						labelCount++;
					}
				} else if (p == 1 && i == 0) {
					if (multi[i][l - 1] != 0) {
						multi[i][l] = multi[i][l - 1];
					} else {
						multi[i][l] = labelCount;
						labelCount++;
					}
				} else if (p == 1 && i != 0 && l != 0) {
					if (multi[i][l - 1] != 0 && multi[i - 1][l] != 0) {
						if (multi[i - 1][l] > multi[i][l - 1]) {
							multi[i][l] = multi[i][l - 1];
							x.put(multi[i - 1][l], multi[i][l - 1]);
						} else if (multi[i - 1][l] < multi[i][l - 1]) {
							multi[i][l] = multi[i - 1][l];
							x.put(multi[i][l - 1], multi[i - 1][l]);
						} else {
							multi[i][l] = multi[i - 1][l];
						}

					} else if (multi[i][l - 1] == 0 && multi[i - 1][l] != 0) {
						multi[i][l] = multi[i - 1][l];
					} else if (multi[i - 1][l] == 0 && multi[i][l - 1] != 0) {
						multi[i][l] = multi[i][l - 1];
					} else {
						multi[i][l] = labelCount;
						labelCount++;
					}
				}

			}
		}
		x.forEach((key, valuse) -> {
			System.out.println(key + "  " + valuse);
		});
		System.out.println("Labelcount " + labelCount + " x " + x.size());
		for (int i = 0; i < 153; i++) {
			for (int l = 0; l < 240; l++) {
				if (x.containsKey(multi[i][l])) {
					multi[i][l] = x.get(multi[i][l]);
				}
			}
		}

		for (int i = 0; i < 153; i++) {
			for (int l = 0; l < 240; l++) {
				if (multi[i][l] != 0) {
					System.out.print(multi[i][l]);
				}

			}
			System.out.println();
		}

	//	HighGui.imshow("Test", result);

		//HighGui.waitKey();
	//	System.exit(0);
	}

	public static void main(String[] args) {
		new Hw2_32();
	}

}
