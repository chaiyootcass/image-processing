package img;

import java.util.HashMap;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class Hw2_33 {

	public Hw2_33() {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		String filename = "/Users/gasprp/eclipse-workspace/img/ani04.jpg";
		Mat src = Imgcodecs.imread(filename, Imgcodecs.IMREAD_GRAYSCALE);// ,Imgcodecs.IMREAD_GRAYSCALE

		if (src.empty()) {
			System.err.println("Cannot read image: " + filename);
			System.exit(0);
		}
		Mat result = src.clone();
		for (int i = 0; i < result.rows(); i++) {
			for (int l = 0; l < result.cols(); l++) {
				double data = result.get(i, l)[0];
				if (data <= 10) {
					data = 0;
				} else {
					data = 255;
				}
				result.put(i, l, data);
			}
		}
		// Imgcodecs.imwrite("Hw2i3_6009610632_9.jpg", result);
		int erosion_size = 1;
		int dilation_size = 1;

		Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT,
				new Size(2 * erosion_size + 1, 2 * erosion_size + 1));
		Mat afterE = new Mat();
		Imgproc.dilate(result, result, element);
		Imgproc.dilate(result, result, element);
		// Imgcodecs.imwrite("Hw2i3_6009610632_10.jpg", result);

		int erosion_size2 = 5;
		int dilation_size2 = 5;

		Mat element2 = Imgproc.getStructuringElement(Imgproc.MORPH_RECT,
				new Size(2 * erosion_size2 + 1, 2 * erosion_size2 + 1));
		for (int i = 0; i < 11; i++) {
			Imgproc.erode(result, result, element2);
		}
		// Imgcodecs.imwrite("Hw2i3_6009610632_11.jpg", result);
		Imgproc.dilate(result, result, element2);
		Imgproc.dilate(result, result, element2);
		Imgproc.dilate(result, result, element2);
		Imgproc.dilate(result, result, element2);
		// Imgcodecs.imwrite("Hw2i3_6009610632_12.jpg", result);

		System.out.println(result.width() + " " + result.height());
		int labelCount = 1;
		HashMap<Integer, Integer> x = new HashMap<Integer, Integer>();
		System.out.println(afterE.width() + " " + afterE.height());

		int[][] multi = new int[78][99];
		for (int i = 0; i < 78; i++) {
			for (int l = 0; l < 99; l++) {
				multi[i][l] = 0;
				for (int i2 = i * 10; i2 < (i * 10) + 10; i2++) {
					for (int l2 = l * 10; l2 < (l * 10) + 10; l2++) {
						if (result.get(i2, l2)[0] <= 150) {
							multi[i][l] = 1;
						}
					}
				}
			}
		}
		for (int i = 0; i < result.height() / 10; i++) {
			for (int l = 0; l < result.width() / 10; l++) {
				int p = multi[i][l];
				if (p == 1 && i == 0 && l == 0) {
					multi[i][l] = labelCount;
					labelCount++;
				} else if (p == 1 && l == 0) {
					if (multi[i - 1][l] != 0) {
						multi[i][l] = multi[i - 1][l];
					} else {
						multi[i][l] = labelCount;
						labelCount++;
					}
				} else if (p == 1 && i == 0) {
					if (multi[i][l - 1] != 0) {
						multi[i][l] = multi[i][l - 1];
					} else {
						multi[i][l] = labelCount;
						labelCount++;
					}
				} else if (p == 1 && i != 0 && l != 0) {
					if (multi[i][l - 1] != 0 && multi[i - 1][l] != 0) {
						if (multi[i - 1][l] > multi[i][l - 1]) {
							multi[i][l] = multi[i][l - 1];
							x.put(multi[i - 1][l], multi[i][l - 1]);
						} else if (multi[i - 1][l] < multi[i][l - 1]) {
							multi[i][l] = multi[i - 1][l];
							x.put(multi[i][l - 1], multi[i - 1][l]);
						} else {
							multi[i][l] = multi[i - 1][l];
						}

					} else if (multi[i][l - 1] == 0 && multi[i - 1][l] != 0) {
						multi[i][l] = multi[i - 1][l];
					} else if (multi[i - 1][l] == 0 && multi[i][l - 1] != 0) {
						multi[i][l] = multi[i][l - 1];
					} else {
						multi[i][l] = labelCount;
						labelCount++;
					}
				}

			}
		}
		x.forEach((key, valuse) -> {
			System.out.println(key + "->" + valuse);
		});
		System.out.println("Labelcount " + labelCount + " x " + x.size());
		for (int z = 0; z < x.size(); z++) {
			for (int i = 0; i < 78; i++) {
				for (int l = 0; l < 99; l++) {
					if (x.containsKey(multi[i][l])) {
						multi[i][l] = x.get(multi[i][l]);
					}
				}
			}
		}

		for (int i = 0; i < 78; i++) {
			for (int l = 0; l < 99; l++) {

				System.out.print(multi[i][l]);

			}
			System.out.println();
		}

		// HighGui.imshow("Test", result);

		// HighGui.waitKey();
		// System.exit(0);
	}

	public static void main(String[] args) {
		new Hw2_33();
	}

}
