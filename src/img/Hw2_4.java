package img;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.CLAHE;
import org.opencv.imgproc.Imgproc;


public class Hw2_4 {
	public void applyCLAHE(Mat srcArry, Mat dstArry) { 
	    //Function that applies the CLAHE algorithm to "dstArry".

	    if (srcArry.channels() >= 3) {
	        // READ RGB color image and convert it to Lab
	        Mat channel = new Mat();
	        Imgproc.cvtColor(srcArry, dstArry, Imgproc.COLOR_BGR2Lab);

	        // Extract the L channel
	        Core.extractChannel(dstArry, channel, 0);

	        // apply the CLAHE algorithm to the L channel
	        CLAHE clahe = Imgproc.createCLAHE();
	        clahe.setClipLimit(4);
	        clahe.apply(channel, channel);

	        // Merge the the color planes back into an Lab image
	        Core.insertChannel(channel, dstArry, 0);

	        // convert back to RGB
	        Imgproc.cvtColor(dstArry, dstArry, Imgproc.COLOR_Lab2BGR);

	        // Temporary Mat not reused, so release from memory.
	        channel.release();
	    }

	}
	private Mat doSobel(Mat frame) {
	    // init
	    Mat grayImage = new Mat();
	    Mat detectedEdges = new Mat();
	    int scale = 1;
	    int delta = 0;
	    int ddepth = CvType.CV_16S;
	    Mat grad_x = new Mat();
	    Mat grad_y = new Mat();
	    Mat abs_grad_x = new Mat();
	    Mat abs_grad_y = new Mat();

	    // reduce noise with a 3x3 kernel
	   // Imgproc.GaussianBlur(frame, frame, new Size(3, 3), 0, 0, Core.BORDER_DEFAULT);

	    // convert to grayscale
	    Imgproc.cvtColor(frame, grayImage, Imgproc.COLOR_BGR2GRAY);

	    // Gradient X
	    // Imgproc.Sobel(grayImage, grad_x, ddepth, 1, 0, 3, scale,
	    // this.threshold.getValue(), Core.BORDER_DEFAULT );
	    Imgproc.Sobel(grayImage, grad_x, ddepth, 1, 0);
	    Core.convertScaleAbs(grad_x, abs_grad_x);

	    // Gradient Y
	    // Imgproc.Sobel(grayImage, grad_y, ddepth, 0, 1, 3, scale,
	    // this.threshold.getValue(), Core.BORDER_DEFAULT );
	    Imgproc.Sobel(grayImage, grad_y, ddepth, 0, 1);
	    Core.convertScaleAbs(grad_y, abs_grad_y);

	    // Total Gradient (approximate)
	    Core.addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0, detectedEdges);
	    // Core.addWeighted(grad_x, 0.5, grad_y, 0.5, 0, detectedEdges);

	    return detectedEdges;

	}
	public Hw2_4() {

	
		//performSUACE(frame, suaceResult, a, (b + 1) / 8.0)
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		//String filename = "/Users/gasprp/eclipse-workspace/img/ani01.jpg";
		String filename = "/Users/gasprp/eclipse-workspace/img/retina1.jpg";
		
		Mat src = Imgcodecs.imread(filename);//,Imgcodecs.IMREAD_GRAYSCALE
		if (src.empty()) {
			System.err.println("Cannot read image: " + filename);
			System.exit(0);
		}
		Mat dst = new Mat(src.size(),CvType.CV_8UC1);
		Mat smoot=new Mat();
		Imgproc.GaussianBlur(src, smoot, new Size(0,0), sigma);
		
		
//		Mat result = new Mat();
//		applyCLAHE(src, result);
	    //Imgproc.cvtColor(result, result, Imgproc.COLOR_RGB2GRAY); 
	   // Imgproc.cvtColor(src, src, Imgproc.COLOR_RGB2GRAY); 
	    
//	    
//	    Imgproc.blur(result, result, new Size(3,3));
//	    Core.subtract(result, src, result);
//	    Imgproc.cvtColor(result, result, Imgproc.COLOR_RGB2GRAY); 
//	    Core.bitwise_not(result, result);
	    
	    
	  
		
	
//		Mat afterL=result.clone();
//		Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE,
//				new Size(2 * 1 + 1, 2 * 1 + 1));
//		
//		Imgproc.dilate(result, result, element);
//		Imgproc.GaussianBlur(result, result, new Size(3, 3), 0, 0, Core.BORDER_DEFAULT);
		//Imgproc.dilate(result, result, element);
		//Imgproc.dilate(result, result, element);
//	Imgproc.Canny(result, result, 5, 10);
		
	//	Imgproc.HoughCircles(result, circle, Imgproc.CV_HOUGH_GRADIENT, 1, 20, 70, 30, 3, 100);
		//Imgproc.HoughCircles(result, circle, Imgproc.CV_HOUGH_GRADIENT, result.depth(),30);
	
		
		
		
		
		//Imgproc.Canny(result, result, 0, 10);
		
		
      // Core.subtract(src,result, result2);
	      // Imgproc.Laplacian(result, result, -1);
	     
      	
		//HighGui.imshow("Test", afterL);
	    Imgproc.resize(dst, dst, new Size(750,500),0,0,Imgproc.INTER_AREA);
		HighGui.imshow("Test1", dst);

		//	Imgcodecs.imwrite("sad.jpg", result);
			HighGui.waitKey();
			System.exit(0);
	}
	public static void main(String[] args) {
		new Hw2_4();
	}

}
