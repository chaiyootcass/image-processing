package img;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class Hw2_51 {

	public Hw2_51() {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		
		String filename = "/Users/gasprp/eclipse-workspace/img/CsAtTu.bmp";
		Mat src = Imgcodecs.imread(filename,Imgcodecs.IMREAD_GRAYSCALE);
		if (src.empty()) {
			System.err.println("Cannot read image: " + filename);
			System.exit(0);
		}
		Mat result = src.clone();
		
		Mat d1=new Mat();
		int erosion_size = 1;
        int dilation_size =1;
        
        Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new  Size(2*erosion_size + 1, 2*erosion_size+1));
        Imgproc.erode(result, d1, element);
       // Imgcodecs.imwrite("Hw2i5_6009610632_1.jpg", d1);
        Mat resultD=new Mat();
        Core.subtract(result, d1, resultD);
      //  Imgcodecs.imwrite("Hw2i5_6009610632_2.jpg", d1);
        Core.bitwise_not(resultD, resultD);
      //  Imgcodecs.imwrite("Hw2i5_6009610632_3.jpg", resultD);
		HighGui.imshow("Test", resultD);

		//	Imgcodecs.imwrite("result5.jpg", result);
		HighGui.waitKey();
		System.exit(0);
	}
	public static void main(String[] args) {
		new Hw2_51();
	}

}
