package img;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class Hw2_52 {
    private static final int MAX_THRESHOLD = 255;
    private int threshold = 100;
    private Random rng = new Random(12345);
	public Mat getHoughPTransform(Mat image, double rho, double theta, int threshold) {
	    Mat result = image.clone();
	    Mat lines = new Mat();
	    Imgproc.HoughLinesP(image, lines, rho, theta, threshold);

	    for (int i = 0; i < lines.cols(); i++) {
	        double[] val = lines.get(0, i);
	        Imgproc.line(result, new Point(val[0], val[1]), new Point(val[2], val[3]), new Scalar(0, 0, 255), 2);
	    }
	    return result;
	}
	public Hw2_52() {
System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		
		String filename = "/Users/gasprp/eclipse-workspace/img/u.png";
		Mat src = Imgcodecs.imread(filename,Imgproc.COLOR_BGR2GRAY);
		if (src.empty()) {
			System.err.println("Cannot read image: " + filename);
			System.exit(0);
		}
		Mat result = src.clone();
		System.out.println(result.width()+" "+result.height());
		Core.bitwise_not(result, result);
		int count=0;
		for (int i = 0; i < result.rows(); i++) {
			for (int l = 0; l < result.cols(); l++) {
				double data = result.get(i, l)[0];
				if(data==0) {
					count++;
				}
			//	result.put(i, l, data);
			}
		}
		System.out.println("Count Pixel :"+count);
		Mat d1=new Mat();
		int erosion_size = 1;
        int dilation_size =1;
        
        Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new  Size(2*erosion_size + 1, 2*erosion_size+1));
        Imgproc.erode(result, d1, element);    
        Core.subtract(result, d1, d1);
       // Core.bitwise_not(d1, d1);
		int countC=0;
		for (int i = 0; i < d1.rows(); i++) {
			for (int l = 0; l < d1.cols(); l++) {
				double data = d1.get(i, l)[0];
				if(data>=115) {
					countC++;
				}
			//	result.put(i, l, data);
			}
		}
		Imgcodecs.imwrite("sad.jpg", d1);
		System.out.println("CountEdge Pixel :"+countC);
//		Mat d1=new Mat();
        
        
        
//        Mat resultD=new Mat();
//        Core.subtract(result, d1, resultD);
//        Core.bitwise_not(resultD, resultD);
		
		HighGui.imshow("Test",d1);
		HighGui.waitKey();
		System.exit(0);
	}
	public static void main(String[] args) {
		new Hw2_52();
	}

}
