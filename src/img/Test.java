package img;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;



public class Test {
	public static Mat norm_0_255(Mat src) {
		
		Mat dst = new Mat();
		
		return dst;
	}
	public void showHis(String filename) {
		//String filename="/Users/gasprp/eclipse-workspace/img/image2.jpg";
        Mat src = Imgcodecs.imread(filename);
        if (src.empty()) {
            System.err.println("Cannot read image: " + filename);
            System.exit(0);
        }
        List<Mat> bgrPlanes = new ArrayList<>();
        Core.split(src, bgrPlanes);
        int histSize = 256;
        float[] range = {10, 256}; //the upper boundary is exclusive
        MatOfFloat histRange = new MatOfFloat(range);
        boolean accumulate = false;
        Mat bHist = new Mat(), gHist = new Mat(), rHist = new Mat();
        Imgproc.calcHist(bgrPlanes, new MatOfInt(0), new Mat(), bHist, new MatOfInt(histSize), histRange, accumulate);
        Imgproc.calcHist(bgrPlanes, new MatOfInt(1), new Mat(), gHist, new MatOfInt(histSize), histRange, accumulate);
        Imgproc.calcHist(bgrPlanes, new MatOfInt(2), new Mat(), rHist, new MatOfInt(histSize), histRange, accumulate);
        int histW = 600, histH = 400;
        int binW = (int) Math.round((double) histW / histSize);
        Mat histImage = new Mat( histH, histW, CvType.CV_8UC3, new Scalar( 0,0,0) );
        Core.normalize(bHist, bHist, 0, histImage.rows(), Core.NORM_MINMAX);
        Core.normalize(gHist, gHist, 0, histImage.rows(), Core.NORM_MINMAX);
        Core.normalize(rHist, rHist, 0, histImage.rows(), Core.NORM_MINMAX);
       
        float[] bHistData = new float[(int) (bHist.total() * bHist.channels())];
        bHist.get(0, 0, bHistData);
        float[] gHistData = new float[(int) (gHist.total() * gHist.channels())];
        gHist.get(0, 0, gHistData);
        float[] rHistData = new float[(int) (rHist.total() * rHist.channels())];
        rHist.get(0, 0, rHistData);
        for( int i = 1; i < histSize; i++) {
        //	 Imgproc.line(histImage, new Point(binW * (i - 1), histH - Math.round(bHistData[i - 1])),
          //           new Point(binW * (i), histH - checkP(Math.round(bHistData[i - 1]),Math.round(bHistData[i]))), new Scalar(255, 0, 0), 1);
           Imgproc.line(histImage, new Point(binW * (i - 1), histH - Math.round(bHistData[i - 1])),
                   new Point(binW * (i), histH - Math.round(bHistData[i])), new Scalar(255, 0, 0), 2);
           // System.out.println( Math.round(bHistData[i - 1])+"  "+ Math.round(bHistData[i])+""
           // 		+ " "+checkP(Math.round(bHistData[i - 1]),Math.round(bHistData[i])));
            
         //   System.out.println();
//            Imgproc.line(histImage, new Point(binW * (i - 1), histH - Math.round(gHistData[i - 1])),
//                    new Point(binW * (i), histH - Math.round(gHistData[i])), new Scalar(0, 255, 0), 2);
//            Imgproc.line(histImage, new Point(binW * (i - 1), histH - Math.round(rHistData[i - 1])),
//                    new Point(binW * (i), histH - Math.round(rHistData[i])), new Scalar(0, 0, 255), 2);
        }
        HighGui.imshow( "Source image", src );
        HighGui.imshow( "calcHist ", histImage );
        Imgcodecs.imwrite("His3.jpg", histImage);
        HighGui.waitKey(0);
	}
	public int checkP(int p1,int p2) {
		
		if(Math.abs(p1-p2)>70) {
			System.out.println(p1+" "+p2);
			return p1>p2?p2:p1;
		}else {
			return p2;
		}
	}
	 
	public Test() {
		/// Users/gasprp/eclipse-workspace/img/Hw1_1A.JPG
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		Mat mat = Imgcodecs.imread("/Users/gasprp/eclipse-workspace/img/image2.jpg",Imgcodecs.IMREAD_GRAYSCALE);
		Mat result = new Mat();
		
        System.exit(0);
//	         Mat kern = new Mat(3, 3, CvType.CV_8S);
//	         int row = 0, col = 0;
//	         kern.put(row, col, 0, -1, 0, -1, 5, -1, 0, -1, 0);
//		Mat result2 = new Mat();
//		 Imgproc.filter2D(mat, result2, mat.depth(), kern);
//		 Mat result = result2.clone();
		

	//	Core.bitwise_and(src1, src2, dst);
	//	Imgproc.medianBlur(result, result2, 15);

	//	Imgproc.equalizeHist(mat, result);
	//	Imgproc.GaussianBlur(mat, result, new Size(0,0), 3);
		
		//HighGui.imshow("Test2", result2);
//		HighGui.imshow("Test3", result3);
		
		//Core.bitwise_not(result, result3, result2);
		
	
	//	Core.addWeighted(mat, 1.5, result, -0.5, 0, result);
		
		//Imgproc.Sobel(result, result2, -1, 1, 1);
	//	Imgproc.medianBlur(mat, result, 5);
		//Core.addWeighted(mat, 1.5, result, -0.5, 0, result);
		//Imgproc.Laplacian(mat, result, 20);
		//Imgproc.Canny(result, result2, 20, 40);
		//Imgproc.GaussianBlur(result, result, new Size(9,9), 0);
		
		
//		Mat source = Imgcodecs.imread("/Users/gasprp/eclipse-workspace/img/Hw1_2A.jpg",Imgcodecs.IMREAD_GRAYSCALE);
//        Mat destination = new Mat(source.rows(),source.cols(),source.type());
//      
//      Imgproc.medianBlur(source,destination, 3);
 
//	     Imgproc.filter2D(source, result, -5, kernel);
//	    Imgproc.Canny(source, destination, 20, 40);
//		HighGui.imshow("Test", result);
//		
//		HighGui.waitKey();
//		System.exit(0);

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Test();

	}

}
