package img;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class whatisThisA {

	public whatisThisA() {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

		Mat mat = Imgcodecs.imread("/Users/gasprp/eclipse-workspace/img/whatIsThisA.jpg", Imgcodecs.IMREAD_GRAYSCALE);
		Mat plusB = new Mat();
		mat.convertTo(plusB, -1,18, 150);
		
		//Imgcodecs.imwrite("whatIsThisA_6009610632_1.jpg", plusB);
		
		Mat blur = new Mat();
		Imgproc.blur(plusB, blur, new Size(5,5));
		
		//Imgcodecs.imwrite("whatIsThisA_6009610632_2.jpg", blur);
		
		 Mat kern = new Mat(3, 3, CvType.CV_8S);
         int row = 0, col = 0;
         kern.put(row, col, 0, -1, 0, -1, 5, -1, 0, -1, 0);
         Mat result = new Mat();
         Imgproc.filter2D(blur, result, blur.depth(), kern);
         
     	//Imgcodecs.imwrite("whatIsThisA_6009610632_3.jpg", result);
         
		 HighGui.imshow("result", result);
		
		HighGui.waitKey();
		System.exit(0);
	}

	public static void main(String[] args) {
		new whatisThisA();

	}

}
