package project;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class ActRecongnition {

	public ActRecongnition(String fileName) {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		Random rng = new Random(12345);
		Mat im = Imgcodecs.imread(fileName);
		Mat x = im.clone();
		if (x.width() > x.height() * 1.5) {
			System.out.println("Sleep");
			return;
		}
		// HighGui.imshow("Select", x);
		Mat frame = new Mat();
		Imgproc.cvtColor(x, frame, Imgproc.COLOR_BGR2HSV);
		Imgproc.GaussianBlur(frame, frame, new Size(7, 7), 1, 1);

		for (int i = 0; i < frame.rows(); i++) {
			for (int l = 0; l < frame.cols(); l++) {
				if (frame.get(i, l)[0] > 5 && frame.get(i, l)[0] < 17 && frame.get(i, l)[1] > 38
						&& frame.get(i, l)[1] < 250 && frame.get(i, l)[2] > 51 && frame.get(i, l)[2] < 242) {
				} else {
					double[] data = { 0, 0, 0 };
					frame.put(i, l, data);
				}
			}
		}
		Mat frameg = new Mat();
		Imgproc.cvtColor(frame, frame, Imgproc.COLOR_HSV2BGR);
		// HighGui.imshow("HSV TO BGR", frame);
		Imgproc.cvtColor(frame, frameg, Imgproc.COLOR_BGR2GRAY);
		Imgproc.threshold(frameg, frameg, 60, 255, Imgproc.THRESH_BINARY);
		Imgproc.medianBlur(frameg, frameg, 15);
		// HighGui.imshow("BINARY", frame);
		int countForArmLeft = 0, countForArmRight = 0, countForHead = 0, allpixel = frameg.width() * frameg.height();
		int h = (int) (frameg.height() * 0.4);
		int w = frameg.width() - (int) (frameg.width() * 0.6);
		//System.out.println("size:" + frameg.size() + " w:" + w + "  " + "h:" + h);
		Rect aL = new Rect((int) (frameg.width() * 0.6), 0, w, h);
		for (int i = (int) (frameg.width() * 0.6); i < frameg.width(); i++) {
			for (int l = 0; l < h; l++) {
				if (frameg.get(l, i)[0] == 255) {
					countForArmLeft++;
				}
			}
		}
		Rect aR = new Rect(0, 0, w, h);
		for (int i = 0; i < w; i++) {
			for (int l = 0; l < h; l++) {
				if (frameg.get(l, i)[0] == 255) {
					countForArmRight++;
				}
			}
		}
//		System.out.println("ArmLeft:" + ((float) countForArmLeft / (float) allpixel) * 100 + " ArmRight" +
//
//				((float) countForArmRight / (float) allpixel) * 100 + " Allp:" + frameg.size());
		Rect head = new Rect((int) (frameg.width() * 0.3), 0, (int) (frameg.width() * 0.35),
				(int) (frameg.height() * 0.35));
		for (int i = (int) (frameg.width() * 0.3); i < (int) (frameg.width() * 0.3)
				+ (int) (frameg.width() * 0.35); i++) {
			for (int l = 0; l < (int) (frameg.height() * 0.35); l++) {
				if (frameg.get(l, i)[0] == 255) {
					countForHead++;
				}
			}
		}
		float pHead = ((float) countForHead / (float) allpixel) * 100;
		//System.out.println("head:" + countForHead + " %head:" + pHead);
		Mat cannyOutput = new Mat();
		cannyOutput = frameg.clone();
		Core.bitwise_not(cannyOutput, cannyOutput);
		// HighGui.imshow("FOR contour", cannyOutput);
		List<MatOfPoint> contours = new ArrayList<>();
		Mat hierarchy = new Mat();
		Imgproc.findContours(cannyOutput, contours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);
		MatOfPoint2f[] contoursPoly = new MatOfPoint2f[contours.size()];
		ArrayList<Rect> topRect = new ArrayList<>();
		Rect[] boundRect = new Rect[contours.size()];
		Point[] centers = new Point[contours.size()];
		float[][] radius = new float[contours.size()][1];
		for (int i = 0; i < contours.size(); i++) {
			contoursPoly[i] = new MatOfPoint2f();
			Imgproc.approxPolyDP(new MatOfPoint2f(contours.get(i).toArray()), contoursPoly[i], 3, true);
			boundRect[i] = Imgproc.boundingRect(new MatOfPoint(contoursPoly[i].toArray()));
			topRect.add(boundRect[i]);
			centers[i] = new Point();
			Imgproc.minEnclosingCircle(contoursPoly[i], centers[i], radius[i]);
		}
		Mat drawing = Mat.zeros(cannyOutput.size(), CvType.CV_8UC3);
		List<MatOfPoint> contoursPolyList = new ArrayList<>(contoursPoly.length);
		for (MatOfPoint2f poly : contoursPoly) {
			contoursPolyList.add(new MatOfPoint(poly.toArray()));
		}
//		double topAreaRect=boundRect[0].area();
//		for (int i = 0; i < contours.size(); i++) {
		// Scalar color = new Scalar(rng.nextInt(256), rng.nextInt(256),
		// rng.nextInt(256));
		// Imgproc.rectangle(drawing, boundRect[i].tl(), boundRect[i].br(), color, 2);
//			System.out.println(
//					boundRect[i].tl().toString() + " " + boundRect[i].br().toString() + " " + boundRect[i].area());
//			if(boundRect[i].area()>topAreaRect) {
//				topAreaRect=boundRect[i].area();
//			}
		// }
		if (topRect.size() > 4) {
			while (topRect.size() != 4) {
				double min = topRect.get(0).area();
				int num = 0;
				for (int i = 0; i < topRect.size(); i++) {
					if (topRect.get(i).area() < min) {
						min = topRect.get(i).area();
						num = i;
					}
				}
				topRect.remove(num);
			}
		}
		for (int i = 0; i < topRect.size(); i++) {
			if (topRect.get(i).area() >= (frameg.size().area() * 0.8)) {
				topRect.remove(i);
			}
		}
		Imgproc.cvtColor(frameg, frameg, Imgproc.COLOR_GRAY2RGB);
		// Imgcodecs.imwrite("frameg.jpg", frameg);
		// HighGui.imshow("test", drawing);
		// System.out.println("aL "+aL.x+" "+aL.y+" "+aL.width);
		int rectArrayRight = -1, countForArmRight2 = 0;
		for (int i = 0; i < topRect.size(); i++) {
			if ((topRect.get(i).area()) > (frameg.size().area() * 0.09) && topRect.get(i).x >= aL.x
					&& topRect.get(i).y <= aL.height) {
				rectArrayRight = i;
			}
		}
		if (rectArrayRight != -1) {
			for (int i = topRect.get(rectArrayRight).x; i < topRect.get(rectArrayRight).x
					+ topRect.get(rectArrayRight).width; i++) {
				for (int l = topRect.get(rectArrayRight).y; l < topRect.get(rectArrayRight).y
						+ topRect.get(rectArrayRight).height; l++) {
					if (frameg.get(l, i)[0] == 255) {
						countForArmRight2++;
					}
//					double[] data = {255.0,255.0,255.0};
//					frameg.put(l, i, data);

				}
			}
		}
		int rectArrayLeft = -1, countForArmLeft2 = 0;
		for (int i = 0; i < topRect.size(); i++) {
			if ((topRect.get(i).y <= aR.height && (topRect.get(i).x <= aR.width))
					&& topRect.get(i).area() >= (frameg.size().area() * 0.08)) {
				rectArrayLeft = i;
			}
		}
		if (rectArrayLeft != -1) {
			for (int i = topRect.get(rectArrayLeft).x; i < topRect.get(rectArrayLeft).x
					+ topRect.get(rectArrayLeft).width; i++) {
				for (int l = topRect.get(rectArrayLeft).y; l < topRect.get(rectArrayLeft).y
						+ topRect.get(rectArrayLeft).height; l++) {
					if (frameg.get(l, i)[0] == 255) {
						countForArmLeft2++;
					}
//					double[] data = {255.0,255.0,255.0};
//					frameg.put(l, i, data);
				}
			}
		}
		// if want to show last process  uncommand below and **
//		for (Rect r : topRect) {
//			Scalar color = new Scalar(rng.nextInt(256), rng.nextInt(256), rng.nextInt(256));
//			Imgproc.rectangle(frameg, r.tl(), r.br(), color, 2);
//		}
//		Imgproc.rectangle(frameg, aL, new Scalar(rng.nextInt(256), rng.nextInt(256), rng.nextInt(256)), 3);
//		Imgproc.rectangle(frameg, aR, new Scalar(rng.nextInt(256), rng.nextInt(256), rng.nextInt(256)), 3);
//		Imgproc.rectangle(frameg, head, new Scalar(rng.nextInt(256), rng.nextInt(256), rng.nextInt(256)), 3);
//		HighGui.imshow("Result", frameg);
//		
//		System.out.println("R2: " + ((float) countForArmLeft2 / (float) allpixel) * 100 + " real: "
//				+ ((float) countForArmRight / (float) allpixel) * 100);
//		System.out.println("L2: " + ((float) countForArmRight2 / (float) allpixel) * 100 + " real: "
//				+ ((float) countForArmLeft / (float) allpixel) * 100);
		if (pHead >= 4) {
			if (frameg.height() > frameg.width() * 1.5) {
				System.out.println("Stand");
			} else {
				System.out.println("Sit");
			}
		} else {
			if(( ((float) countForArmLeft / (float) allpixel) * 100)<=0.1&&(((float) countForArmRight / (float) allpixel) * 100)<=0.1){
				System.out.println("Sleep");
			}else {
				if((((float) countForArmRight / (float) allpixel) * 100)>(((float) countForArmLeft / (float) allpixel) * 100)) {
					if(((float) countForArmRight2 / (float) allpixel) * 100>4) {
						System.out.println("Left");
					}else {
						System.out.println("Right");
					}
					
				}else {
					if(((float) countForArmLeft2 / (float) allpixel) * 100>4) {
						System.out.println("Right");
					}else {
						System.out.println("Left");
					}
				}
			}
			
		
		}
		
	//and this **
//		HighGui.waitKey();
//		System.exit(0);
	}
	public static void main(String[] args) {
		new ActRecongnition("/Users/gasprp/Desktop/projectpic/sleep/sleepOnly1.jpeg-objects/person-1.jpg");
		
	}
}
